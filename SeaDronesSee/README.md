Este directorio contiene los resultados de entrenamiento del modelo de red neuronal "SeaDronesSee", por tanto, hay tres carpetas (train, train2 y train3). El entrenamiento se ha hecho con las versiones de Yolov8 (n-s-m), así mismo, este modelo en versión Yolov8 se ha convertido a formato openvino (IR),
donde la carpeta weights contiene las versiones onnx, bin y xml.
