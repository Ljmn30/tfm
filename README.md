# TFM - DISEÑO Y ACELERACIÓN DE UN SISTEMA DE VISIÓN PARA VEHÍCULOS ACUÁTICOS NO TRIPULADOS

Autor: José Luis Mela Navarro

Este repositorio contiene el trabajo que ha desarrollado para el TFM que tiene como título mencionado arriba.

## Resumen

Este proyecto inicia con el objetivo de evaluar el rendimiento de modelos de redes neuronales en diferente hardware, específicamente enfocados a ejecutar tareas de visión por computador.

Donde se inició con la búsqueda de conjunto de datos vinculado a entorno acuático para entrenar las versiones nano, small y medium de Yolov8 para tres conjunto de datos. 

Por lo anterior, los conjuntos de datos que se han utilizando para crear los modelos de redes neuronales se encuentran en los siguientes enlaces:

- [Datasense@CRAS](https://universe.roboflow.com/cnnonboard/boat-detection-model)
- [SeaDroneSee](https://universe.roboflow.com/cnnaerialview/boat-detection-model-zm5ac)
- [USVDDModel](https://universe.roboflow.com/modelboat/boat-detection-oelpk)

Los dos primeros dataset son resultados de proyectos de investigación, que fueron descargados y cargados en la plataforma roboflow para generar versiones preentrenadas con el modelo de Yolo, específicamente la versiones Yolov8n, Yolov8s y Yolov8m.
El último dataset son imágenes y sus respectivas anotaciones descargadas del repositorio OpenImagenes V6, que para este estudio consiste en nuestra contribución, donde se ha utilizado el siguiente [tutorial](https://youtu.be/U2V2tUwquAA) para descargar las imágenes y sus anotaciones correspondientes desde el entorno de Google Colab.

Una vez se tuvo los tres modelos para cada dataset se aplicó conversión a ONNX y a formato de representación intermedia (IR), para aplicar métodos de cuantización ofrecidos por el framework OpenVino, donde posteriormente se realizó la implementación de estos modelos a nivel de CPU, GPU y MIRIADs en el entorno Intel Edge, evaluando el rendimiento y consumo energético. Así mismo, fueron evaluados los modelos en formato ONNX en los dispositivos Nvidia Jetson Orin Nano y AGX.


## Hallazgos

El diseño y aceleración de un sistema de visión para vehículos acuáticos no tripulados se basa en la creación de los modelos de redes neuronales hasta el despliegue de los mismos. Por tanto, en este trabajo se ha realizado la búsqueda de conjunto de datos para crear modelos de redes neuronales para la detección de objetos en entornos acuáticos. En este sentido, se ha realizado entrenamiento con la versión del modelo Yolov8 por su tiempo de inferencia y precisión, logrando obtener de acuerdo a las métricas presentadas en la memoria un rendimiento bastante alto.

Por lo anterior, hemos detectado en la fase de entrenamiento que si el modelo no llega a tener un rendimiento en forma escalable durante un número de épocas el algoritmo optimizador SGD de Yolov8 terminará dicho proceso automáticamente, aunque no se haya cumplido el total de épocas definidas por el usuario para entrenar la red, por lo que es eficiente para optimizar tiempo y recursos. En efecto, se ha encontrado que la versión para Yolov8 que presenta mejor rendimiento es la versión Yolov8n, es decir la nano, además es la que toma un menor tiempo durante la fase de entrenamiento.

Al convertir los modelos de redes neuronales de formato Yolov8 a formato IR de OpenVino se descubrieron varios hallazgos como: 1) en el caso del framework OpenVino se tuvo que utilizar la versión 2022.3.1, ya que está era compatible con el hardware que se quería probar, 2) para convertir el modelo en formato pt a IR utilizando model optimizer se tuvo que definir el parámetro imgsz=[448,640], debido a que para ejecutar estos modelos en dispositivos MYRIADs, los mismos tienen restricciones con el tamaño o dimensión de las imágenes de entrada.

Vinculado a lo anterior, para aplicar métodos de cuantización, especifícamente optmización a los modelos de redes neuronales por medio de OpenVino, se comprueba que con el método model optimizer se llega a obtener un alto rendimiento en el modelo sin perder precisión. En cambio con POT de acuerdo a los test realizados se pierde mucha presición, un 35 % en términos de rendimiento, comparado con model optimizer, además por la documentación se menciona que ha sido descontinuada. Otro método de optmización utilizado en este proyecto fue NNCF, donde se convirtió el modelo de formato FP32 a INT8, destacando que los modelos no presentaron pérdida de precisión significativa, pero si ganancia en rendimiento, específicamente 52 FPS comparando el modelo yolov8n en FP32 con yolov8n en INT8 del dataset Datasense@CRAS. En este sentido, hay que mencionar que NNCF inserta una capa llamada FakeQuantize, que no es soportada por el momento por los plugins que dan soporte a los dispositivos VPU MYRIADs, por lo cual no se pudo comprobar el rendimiento de los modelos en estos aceleradores neuronales.

Para comparar los modelos en cuanto a rendimiento y consumo energético se ha realizado pruebas de benchmarking en Intel edge a nivel de CPU, GPU y VPU MYRIADs, por lo que se destaca que nuestros modelos, versión nano para cada conjunto de datos se llega a obtener mejor rendimiento, así como también menor consumo energético, sin importar el dispositivo donde se ejecutó el proceso. Sin embargo, hay que mencionar que para los tres target o dispositivos donde menos consumo energético se llegó a tener, independientemente del modelo fueron los MYRIADs y de igual manera se obtuvo el rendimiento (FPS) más alto, debido a que este tipo de dispositivos están enfocados para el procesamiento y aceleración de modelos de redes neuronales.

En cuanto a los dispositivos Nvidia Orin, AGX y Nano se ha comprobado en las pruebas de rendimiento desplegadas utilizando precisión FP32, FP16 y INT8 en la ejecución de benchmarking para cada modelo de red neuronal que el dispositivo Orin AGX se obtiene rendimientos más altos, ya que tiene mayor capacidad de procesamiento de acuerdo a la documentación. Donde, la versión nano de los modelos de cada conjunto de datos presentan mejor rendimiento. En efecto, haciendo una comparativa entre ambos dispositivos Nvidia utilizados, tomando como ejemplo la precisión INT8 se obtiene un x2 aproximadamente en el rendimiento de la Orin AGX con respecto a la Nano. No obstante, el dispositivo Nvidia Jetson AGX Orin no es el más adecuado para llevar en el USV por razones de costo.

Sin embargo, haciendo un análisis considerando las similitudes de específicaciones técnicas presentadas entre la Intel edge y la Nvidia Jetson Orin Nano, está última presenta mejor rendimiento y menor consumo energético, por lo que es el dispositivo más óptimo en cuanto relación rendimiento versus precio para el desplegar el sistema que llevará el barco en un futuro.